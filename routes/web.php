<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');



Route::get('/register', 'AuthController@daftar');

Route::post('/welcome', 'AuthController@home');

Route::get('/data-table', function(){
    return view('table.data-table');
});


Route::group(['middleware' => ['auth']], function (){
    //
    // CRUD Cast
    // Create
    Route::get('/cast/create', 'CastController@create'); //Route menuju form create
    Route::post('/cast', 'CastController@store'); //Route untuk menyimpan data baru ke database

    
    //Update
    Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route menampilkan form untuk edit pemain film dengan id tertentu
    Route::put('/cast/{cast_id}', 'CastController@update'); //Route menyimpan perubahan data pemain film (update) untuk id tertentu di database

    //Delete

    Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route menghapus data pemain film dengan id tertentu

});

// Read
Route::get('/cast', 'CastController@index'); //Route menampilkan list data para pemain film
Route::get('/cast/{cast_id}', 'CastController@show'); //Route menampilkan detail data pemain film dengan id tertentu




Auth::routes();

