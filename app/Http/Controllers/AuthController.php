<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
   public function daftar(){
       return view('halaman.register');
   }

   public function home(Request $request){
      $fname = $request['Fnama'];
      $lname = $request['Lnama'];
      return view('halaman.welcome', compact('fname', 'lname'));
     
    }
}
